import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  sideMenuBar:any[]=[false,false,false,false];

  clickMenu(value:any){
    this.sideMenuBar[value]=(this.sideMenuBar[value]) ? false : true;
  }

}
