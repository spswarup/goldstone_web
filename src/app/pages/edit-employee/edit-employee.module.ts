import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditEmployeeRoutingModule } from './edit-employee-routing.module';
import { EditEmployeeComponent } from './edit-employee.component';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { SharedComponentModule } from 'src/app/shared/modules/shared-component/shared-component.module';
import { LayoutModule } from 'src/app/shared/modules/layout/layout.module';


@NgModule({
  declarations: [
    EditEmployeeComponent
  ],
  imports: [
    CommonModule,
    EditEmployeeRoutingModule,
    MaterialModule,
    SharedComponentModule,
    LayoutModule
  ]
})
export class EditEmployeeModule { }
