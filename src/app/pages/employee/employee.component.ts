import { Component, OnInit, TemplateRef } from '@angular/core';
import { ITableColumnHeader } from 'src/app/shared/constants/table-column-header';
import { MatDialog } from '@angular/material/dialog';
import { AddSubMenuComponent } from 'src/app/components/add-sub-menu/add-sub-menu.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor(private dialog: MatDialog ) { }

  ngOnInit(): void {
    
  }
  tableDisplayColumns: any[] = [
    {
      columnName: "Dynamic 1",
      columnProperty: "Name"
    },
    {
      columnName: "Dynamic 2",
      columnProperty: "phone"
    },
    {
      columnName: "Dynamic 3",
      columnProperty: "email"
    },
    {
      columnName: "Dynamic 4",
      columnProperty: "pin"
    },
    {
      columnName: "Dynamic 5",
      columnProperty: "Designaion"
    },
    {
      columnName: "Dynamic 6",
      columnProperty: "dob"
    },
    


  ];
  displayedColumns = ['Designaion', 'Name', 'phone', 'email', 'pin', 'dob','edit','delete','view'];

  tableData:any[]=[
    {Designaion : 'UI Designer' , Name : 'Swarup Poddar', phone : '9434534818', email: 'swarup.psd2html@gmail.com' , pin : '713203' , dob : '03-09-1980'}
    

  ];

  openCreate(){
    const dialogRef = this.dialog.open(AddSubMenuComponent,{
      width: '550px',
      data : {
        imageType : 'profile-picture'
      },
      disableClose : true
    })
  }

}
