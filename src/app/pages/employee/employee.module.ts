import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeComponent } from './employee.component';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { SharedComponentModule } from 'src/app/shared/modules/shared-component/shared-component.module';
import { LayoutModule } from 'src/app/shared/modules/layout/layout.module';


@NgModule({
  declarations: [
    EmployeeComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    MaterialModule,
    SharedComponentModule,
    LayoutModule

  ]
})
export class EmployeeModule { }
