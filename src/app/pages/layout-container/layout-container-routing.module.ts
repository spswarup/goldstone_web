import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAuthGuard } from 'src/app/services/user-auth-guard.service';
import { LayoutContainerComponent } from './layout-container.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutContainerComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate : [UserAuthGuard]
      },
      {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule)
      },
      { path: 'menuone',
        loadChildren: () => import('../menuone/menuone.module').then(m => m.MenuoneModule) },

        { path: 'employee',
        loadChildren: () => import('../employee/employee.module').then(m => m.EmployeeModule)
        },
        { path: 'create-employee',
        loadChildren: () => import('../create-employee/create-employee.module').then(m => m.CreateEmployeeModule) },
        { path: 'view-employee', loadChildren: () => import('../view-employee/view-employee.module').then(m => m.ViewEmployeeModule) },
        { path: 'edit-employee', loadChildren: () => import('../edit-employee/edit-employee.module').then(m => m.EditEmployeeModule) },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
    ]
  },






];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutContainerRoutingModule { }
