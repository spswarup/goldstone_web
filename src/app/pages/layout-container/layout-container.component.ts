import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-layout-container',
  templateUrl: './layout-container.component.html',
  styleUrls: ['./layout-container.component.scss']
})
export class LayoutContainerComponent implements OnInit, OnDestroy {
  isSidebarOpened: boolean = true;
  topSidebarGap: number = 64;
  sidenavMode: MatDrawerMode = 'side';
  private _configSub: Subscription = new Subscription();

  constructor(private layoutService: LayoutService) { }

  ngOnInit(): void {
    this._configSub = this.layoutService.layoutConfig.subscribe(config =>{
      this.isSidebarOpened = !config.matches;
      this.topSidebarGap = config.matches ? 56 : 64;
      this.sidenavMode = config.matches ? 'push' : 'side';
    });
  }
  ngOnDestroy(){
    this._configSub.unsubscribe();
  }

}
