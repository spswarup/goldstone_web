import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutContainerRoutingModule } from './layout-container-routing.module';
import { LayoutContainerComponent } from './layout-container.component';
import { LayoutModule } from 'src/app/shared/modules/layout/layout.module';


@NgModule({
  declarations: [
    LayoutContainerComponent
  ],
  imports: [
    CommonModule,
    LayoutContainerRoutingModule,
    LayoutModule
  ]
})
export class LayoutContainerModule { }
