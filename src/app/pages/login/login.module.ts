import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { SharedComponentModule } from 'src/app/shared/modules/shared-component/shared-component.module';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedComponentModule,
    MaterialModule
  ]
})
export class LoginModule { }
