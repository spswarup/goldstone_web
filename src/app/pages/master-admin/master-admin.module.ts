import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterAdminRoutingModule } from './master-admin-routing.module';
import { MasterAdminComponent } from './master-admin.component';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { SharedComponentModule } from 'src/app/shared/modules/shared-component/shared-component.module';
import { LayoutModule } from 'src/app/shared/modules/layout/layout.module';

@NgModule({
  declarations: [
    MasterAdminComponent
  ],
  imports: [
    CommonModule,
    MasterAdminRoutingModule,
    MaterialModule,
    SharedComponentModule,
    LayoutModule
  ]
})
export class MasterAdminModule { }
