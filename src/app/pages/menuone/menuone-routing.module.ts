import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuoneComponent } from './menuone.component';

const routes: Routes = [{ path: '', component: MenuoneComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuoneRoutingModule { }
