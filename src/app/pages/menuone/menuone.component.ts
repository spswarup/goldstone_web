import { Component, OnInit, TemplateRef } from '@angular/core';
import { ITableColumnHeader } from 'src/app/shared/constants/table-column-header';
import { MatDialog } from '@angular/material/dialog';
import { AddSubMenuComponent } from 'src/app/components/add-sub-menu/add-sub-menu.component';
@Component({
  selector: 'app-menuone',
  templateUrl: './menuone.component.html',
  styleUrls: ['./menuone.component.scss']
})
export class MenuoneComponent implements OnInit {

  constructor(private dialog: MatDialog ) { }

  ngOnInit(): void {
    
  }
  tableDisplayColumns: any[] = [
    {
      columnName: "Dynamic 1",
      columnProperty: "Name1"
    },
    {
      columnName: "Dynamic 2",
      columnProperty: "Name2"
    },
    {
      columnName: "Dynamic 3",
      columnProperty: "Name3"
    },
    {
      columnName: "Dynamic 4",
      columnProperty: "Name4"
    },
    {
      columnName: "Dynamic 5",
      columnProperty: "Name5"
    },
    {
      columnName: "Dynamic 6",
      columnProperty: "Name6"
    },
    {
      columnName: "Dynamic 7",
      columnProperty: "Name7"
    },
    {
      columnName: "Dynamic 8",
      columnProperty: "Name8"
    },


  ];
  displayedColumns = ['Name1', 'Name2', 'Name3', 'Name4', 'Name5','Name6','Name7','Name8','edit','delete','view'];

  tableData:any[]=[
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'},
    {Name1 : 'A', Name2 : 'B', Name3 : 'C', Name4 : 'D' , Name5 : 'E' , Name6 : 'F' , Name7 : 'G' , Name8 : 'H'}

  ];

  openCreate(){
    const dialogRef = this.dialog.open(AddSubMenuComponent,{
      width: '550px',
      data : {
        imageType : 'profile-picture'
      },
      disableClose : true
    })
  }
}
