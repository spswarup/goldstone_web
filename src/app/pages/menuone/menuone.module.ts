import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuoneRoutingModule } from './menuone-routing.module';
import { MenuoneComponent } from './menuone.component';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { SharedComponentModule } from 'src/app/shared/modules/shared-component/shared-component.module';
import { LayoutModule } from 'src/app/shared/modules/layout/layout.module';

@NgModule({
  declarations: [
    MenuoneComponent
  ],
  imports: [
    CommonModule,
    MenuoneRoutingModule,
    MaterialModule,
    SharedComponentModule,
    LayoutModule
  ]
})
export class MenuoneModule { }
