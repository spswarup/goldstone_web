import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewEmployeeRoutingModule } from './view-employee-routing.module';
import { ViewEmployeeComponent } from './view-employee.component';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { SharedComponentModule } from 'src/app/shared/modules/shared-component/shared-component.module';
import { LayoutModule } from 'src/app/shared/modules/layout/layout.module';


@NgModule({
  declarations: [
    ViewEmployeeComponent
  ],
  imports: [
    CommonModule,
    ViewEmployeeRoutingModule,
    MaterialModule,
    SharedComponentModule,
    LayoutModule
  ]
})
export class ViewEmployeeModule { }
