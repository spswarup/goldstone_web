import { Injectable } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  private _layoutConfig: BehaviorSubject<BreakpointState> = new BehaviorSubject<BreakpointState>({matches: false, breakpoints: {}});

  constructor(private _mediaObserver: BreakpointObserver) { 
    this._mediaObserver.observe('(max-width: 991px)').subscribe(state =>{
      this._layoutConfig.next(state);
    });
  }

  get layoutConfig(){
    return this._layoutConfig.asObservable();
  }
}
