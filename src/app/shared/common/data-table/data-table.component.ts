import { AfterViewInit, Component, Input, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ITableColumnHeader } from 'src/app/shared/constants/table-column-header';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DataTableComponent implements OnInit, AfterViewInit {
  private _dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]); 
  @Input() set dataSource(value: any[]){
    this._dataSource = new MatTableDataSource(value)
    this.totalPageItems = value.length;
    this._dataSource.paginator = this.pagination;
  }
  @Input()
  menuDropdownTemplate!: TemplateRef<any>;
  @Input()
  imageTemplate!: TemplateRef<any>;
  @Input() columnHeaders: ITableColumnHeader[] = [];
  @Input() showPagination: boolean = false;
  @Input() showMenuButton: boolean = false;
  @Input() showImageColumn: boolean = false;
  @Input() showFilter: boolean = false;
  @Input() itemsPerPage: number = 10;
  displayedColumns: string[] = [];
  @ViewChild(MatPaginator)
  pagination!: MatPaginator;
  totalPageItems: number = 0;


  constructor() {}

  ngAfterViewInit(){
    this._dataSource.paginator = this.pagination;
  }

  ngOnInit(): void {
    this.displayedColumns = this.columnHeaders.map(column => column.columnProperty).slice();
    if(this.showMenuButton){
      this.displayedColumns.push("menu");
    }
    if(this.showImageColumn){
      this.displayedColumns.unshift("image");
    }
    // console.log(this.columnHeaders, this.displayedColumns);
  }

  onFilterData(ev: Event){
    const filterValue = (ev.target as HTMLInputElement).value;
    this._dataSource.filter = filterValue.trim().toLowerCase();
  }

  get dataSource(){
    return this._dataSource as any;
  }

}
