import { TemplateRef } from "@angular/core";

export interface ITableColumnHeader {
    columnName: string;
    columnProperty: string;
    columnTemplate?: TemplateRef<HTMLElement>;
}
