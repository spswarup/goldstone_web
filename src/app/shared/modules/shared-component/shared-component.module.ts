import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from '../../common/data-table/data-table.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { AddSubMenuComponent } from 'src/app/components/add-sub-menu/add-sub-menu.component';


const customComponents = [
  DataTableComponent,
  AddSubMenuComponent
];
@NgModule({
  declarations: [customComponents],
  imports: [
    CommonModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    customComponents
  ]
})
export class SharedComponentModule { }
